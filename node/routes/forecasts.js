var http    = require('http'),
    mongo   = require('mongodb'),
    mongoose = require('mongoose');

/*=============================================
*               Mongo w/ Mongoose
* ============================================*/
mongoose.connect('mongodb://localhost/forecastsdb');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

var forecastsSchema = mongoose.Schema(
    {
        zip: Number,
        response: { body: String }
    });
var Forecast = mongoose.model('Forecast', forecastsSchema);


/*=============================================
 *               Route Handlers
 * ============================================*/

 exports.findByZipFromCache = function(req, res) {
    var zip = req.params.zip;

    Forecast.findOne({ zip: zip }, function (err, forecast) {
        if (err)
        {
            console.log(err);
        }

        console.log('Forecast cache result: '+forecast);

        if (forecast == null)
        {
            req.params.saveCache = true;
            console.log('NOT in cache: '+zip);
            exports.findByZip(req, res);
        }
        else
        {
            console.log('Found in cache: '+zip);
            res.setHeader('Access-Control-Allow-Origin','*')
            res.send(forecast.response.body);
        }
    });
};

exports.findByZip = function(req, res) {
    var zip = req.params.zip;
    console.log('Retrieving forecast for: ' + zip);

    var options = {
        hostname: 'query.yahooapis.com',
        path: '/v1/public/yql?q=select%20item%20from%20weather.' +
            'forecast%20where%20location%3D%22' + zip + '%22'
    };

    var yahoo = http.get(options,
        function(result) {
            var data = "";
            result.on('data',
                function(chunk)
                {
                    data += chunk;
                }
            );

            result.on('end',
                function()
                {
                    if (req.params.saveCache == true)
                    {
                        Forecast.findOneAndUpdate({ zip: zip },
                            { zip: zip, response: {body:data} },
                            { upsert: true},
                            function(err)
                            {
                                if(err)
                                {
                                    console.log(err);
                                }
                            }
                        );
                        console.log('Saved in cache: '+zip);
                    }
                    //console.log('=====' + data + '=====');
                    res.setHeader('Access-Control-Allow-Origin','*')
                    res.send(data);
                }
            );

            result.on('error',
                function(err)
                {
                    console.log('ERROR: ' + err.message);
                    res.send(err.message);
                }
            );
        }
    );
};
