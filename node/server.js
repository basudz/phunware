var express     = require('express'),
    forecasts   = require('./routes/forecasts');
    app         = express();

app.listen(3000);
app.get('/forecasts/', forecasts.findByZip);
app.get('/forecasts/:zip', forecasts.findByZip);
app.get('/forecasts/cache/:zip', forecasts.findByZipFromCache);

console.log('Express server listening on port 3000.');
