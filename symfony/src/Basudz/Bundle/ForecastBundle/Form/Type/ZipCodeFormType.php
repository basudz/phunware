<?php

namespace Basudz\Bundle\ForecastBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackValidator;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

class ZipCodeFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('zipCode', 'number', array(
            'label'         => 'Zip Code',
            'required'      => false,
            'max_length'    => 5,
            'attr'  => array(
                'placeholder'   => 'Enter Zip Code',
                'class' => 'span2'
                ),
            )
        );

        $builder->add('cacheResult', 'checkbox', array(
            'label'     => 'Use cache',
            'required'  => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zipCodeForm';
    }
}
