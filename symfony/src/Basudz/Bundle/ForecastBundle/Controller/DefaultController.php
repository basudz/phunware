<?php

namespace Basudz\Bundle\ForecastBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Basudz\Bundle\ForecastBundle\Form\Type\ZipCodeFormType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="forecast_index")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $form = $this->createForm(new ZipCodeFormType());
        return array('zipCodeForm' => $form->createView());
    }
}
