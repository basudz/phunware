<?php

namespace Basudz\Bundle\ForecastBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    private $client;
    private $crawler;

    public function __construct()
    {
        $this->client = static::createClient();
        $this->crawler = $this->client->request('GET', '/');
    }

    public function testIndexLoadedNavbar()
    {
        $this->assertTrue(
            $this->crawler->filter('html:contains("Yahoo Weather Forecasts")')
                ->count() > 0
        );
    }

    public function testIndexLoadedZipCodeTextbox()
    {
        $this->assertCount(1,
            $this->crawler->filter('input[id=zipCodeForm_zipCode]')
        );
    }

    public function testIndexLoadedUseCacheCheckbox()
    {
        $this->assertCount(1,
            $this->crawler->filter('input[id=zipCodeForm_cacheResult]')
        );
    }

    public function testIndexLoadedSearchButton()
    {
        $this->assertCount(1,
            $this->crawler->filter('button[id=searchZip]')
        );
    }

    public function testIndexLoadedResultsDiv()
    {
        $this->assertCount(1,
            $this->crawler->filter('div[id=yahooResults]')
        );
    }
}
